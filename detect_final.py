#import library yang digunakan  
import numpy as np
import cv2
from matplotlib import pyplot as plt
import time
import imutils
from imutils.video import VideoStream
import Tkinter as tk
from playsound import playsound
import RPi.GPIO as GPIO
from smbus2 import SMBus
from mlx90614 import MLX90614

#deklarasi pin untuk sensor suhu mlx90416
bus = SMBus(1)
sensor = MLX90614(bus, address=0x5A)

#deklarasi pin untuk solenoid dan setting gpio mode
RELAY = 18
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)
GPIO.setup(12, GPIO.OUT)
GPIO.output(12,GPIO.LOW)
GPIO.setup(23, GPIO.IN)
    
#Fungsi untuk menampilkan frame menggunakan Tkinter 
def show_frame(frame):
    frame.tkraise()
    
#deklarasi window dan attribute
window = tk.Tk()
window.attributes('-zoomed', True)

window.rowconfigure(0, weight=1)
window.columnconfigure(0, weight=1)

#deklarasi tiap frame yang akan digunakan
frame1 = tk.Frame(window)
frame2 = tk.Frame(window)
frame3 = tk.Frame(window)

for frame in (frame1, frame2, frame3):
    frame.grid(row=0,column=0,sticky='nsew')

#Label pada frame 1
frame1_title=  tk.Label(frame1, text='Selamat Datang! Tempatkan wajah Anda di depan Kamera', font='times 35', bg='blue', fg="white")
frame1_title.pack(fill='both', expand=True)

#Button pada frame 1
frame1_btn = tk.Button(frame1, text='Enter',command=lambda:show_frame(frame2))
frame1_btn.pack(fill='x',ipady=15)

#deklarasi kamera
capture = "nvarguscamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, height=(int)720,format=(string)NV12, framerate=(fraction)30/1 ! nvvidconv flip-method=2 ! video/x-raw, format=(string)BGRx ! videoconvert ! video/x-raw, format=(string)BGR ! appsink"

#memanggil mask model hasil training
face_cascade_mask=cv2.CascadeClassifier('cascade_mask.xml')
face_cascade=cv2.CascadeClassifier('cascade_nomask.xml')

#memulai camera
webcam = VideoStream(src=capture).start()

while True:	
	#menampilkan window frame 1
    show_frame(frame1)
    window.after(2000, window.quit) 
    window.mainloop()
    temperature = 0
    condition = ""
    
	#membaca input dari pin 23 yaitu pir sensor
    pir = GPIO.input(23)
    iteration = 0
    isHere = 0

	#jika input bernilai 1 maka terdeteksi gerakan
    if pir == 1:
		isHere  = 1
		isDone =  0
		window.quit
    	while isHere==1:
		frames = webcam.read()
        cv2.imshow('test',frames)
		if cv2.waitKey(1) & 0xFF ==ord('a'):	
            cv2.imwrite(filename='saved_img_mask.jpg', img=frames)
			cv2.destroyAllWindows()
			temperature = sensor.get_object_1()
			frame2_title=  tk.Label(frame2, text='Suhu : '+str(temperature), font='times 35', bg='blue', fg="white")
			frame2_title.pack(fill='both', expand=True)
			
			frame2_btn = tk.Button(frame2, text='Enter',command=lambda:show_frame(frame3))
			frame2_btn.pack(fill='x',ipady=15)
			frame1_btn.invoke()
			window.after(2000, window.quit)
    		window.mainloop()
			isHere = 0

    	img_ = cv2.imread('saved_img_mask.jpg', cv2.IMREAD_ANYCOLOR)
    	img=cv2.imread('saved_img_mask.jpg')
    	gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    	font=cv2.FONT_HERSHEY_SIMPLEX
    	faces=face_cascade.detectMultiScale(gray,1.2,3)
    	faces_mask=face_cascade_mask.detectMultiScale(gray,1.3,5)

    	if temperature <= 37.0:
    		for(x,y,w,h) in faces:
        		img=cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)
        		cv2.putText(img,'No Masked',(x,y),font,0.9,(0,0,255),2)
        		condition = "no_mask"
	
			if isDone == 0:
				frame3_title=  tk.Label(frame3, text='Harap Gunakan masker Anda untuk memasuki gedung', font='times 35', bg='red', fg="white")
				frame3_title.pack(fill='both', expand=True)
			
				frame3_btn = tk.Button(frame3, text='Enter',command=lambda:show_frame(frame1))
				frame3_btn.pack(fill='x',ipady=15)
				frame2_btn.invoke()
				
				window.after(2000, window.quit)
    				window.mainloop()
        			playsound('NoMask.mp3')
				GPIO.output(12,GPIO.LOW)
				isDone =  1

    		for(x,y,w,h) in faces_mask:
        		img=cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
        		cv2.putText(img,'Masked',(x,y),font,0.9,(0,255,0),2)
        		condition = "masked"
			if isDone == 0:
				
				frame3_title=  tk.Label(frame3, text='Sangat bagus! Gunakan masker Anda  saat di dalam gedung', font='times 35', bg='green', fg="white")
				frame3_title.pack(fill='both', expand=True)
			
				frame3_btn = tk.Button(frame3, text='Enter',command=lambda:show_frame(frame1))
				frame3_btn.pack(fill='x',ipady=15)
				frame2_btn.invoke()
				
				window.after(2000, window.quit)
    				window.mainloop()	
				#show_window("Sangat bagus! Gunakan selalu masker Anda  saat di dalam gedung", "green")
        			playsound('Masked.mp3')
				GPIO.output(12,GPIO.HIGH)
				isDone =  1
    	else:
		#show_window("Suhu tubuh Anda berada di atas 37 derajat Celcius", "red")
		frame3_title=  tk.Label(frame3, text='Suhu tubuh Anda berada di atas 37 derajat Celcius', font='times 35', bg='red', fg="white")
		frame3_title.pack(fill='both', expand=True)
			
		frame3_btn = tk.Button(frame3, text='Enter',command=lambda:show_frame(frame1))
		frame3_btn.pack(fill='x',ipady=15)
		frame2_btn.invoke()
		window.after(2000, window.quit)
    		window.mainloop()	

    
    	time.sleep(5)
    	GPIO.output(12,GPIO.LOW)

cv2.waitKey(0)
cv2.destroyAllWindows()
